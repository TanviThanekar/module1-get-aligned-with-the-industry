
Deep Neural Network
DNN which is known as Deep Neural Network. It is an artificial neural network with multiple layers between input and output layers. There are three layers in a DNN :
•	Input Layer
•	Hidden Layer
•	Output Layer
Every neuron has a specific activation number. Weights are assigned to each neuron of first layer. All the activations of the first layer are taken and then compute to the weighted sum of all.

Activation Function : It is a function of a node that defines the output of that node given an input or set of inputs.
Sigmoid Function :
•	Sigmoid function also known as the logistic function.The input to the function is transformed into a value between 0.0 and 1.0. Before applyling the activation function we add bias (constant).
•	Bias neuron is a special neural added to each layer in the neural network.
•	But since , the commonly used activation function is ReLU and it is easy to use so we apply the respective function instead of sigmoid function. The range of the ReLU function between 0 and a ie the activation number which is given , not necessary that it should be denoted by a it can be denoted by any variable.

Gradient Descent : Gradient descent is used to minimize the function by iteratively moving in the direction of steepest descent ie it tells the direction in which you should step to increase the function much faster.

Backpropagation : The procedure of evaluating the expression of the derivative of the cost function as product of derivatives between each layer from left to right ie backwards. It is basically an algorithm used for supervised learning of neural network using the gradient descent.
Steps:

Initiliaze weights and biases in the network.
Propogate the inputs forward (by applyin the activation function).
Backpropogate the error (by updating weights and biases).
Terminating condition(when the error is very small).

Stochastic neural networks are a type of artificial neural networks built by introducing random variations into the network, either by giving the network's neurons stochastic transfer functions, or by giving them stochastic weights. This makes them useful tools for optimization problems, since the random fluctuations help it escape from local minima.