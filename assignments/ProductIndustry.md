**Product name**
**Vision AI**

Product link: [Link](https://cloud.google.com/vision)

**Product Short description**
Derive insights from your images in the cloud or at the edge with AutoML Vision or use pre-trained Vision API models to detect emotion, understand text, and more.

**AutoML Vision**
Automate the training of your own custom machine learning models. Simply upload images and train custom image models with AutoML Vision’s easy-to-use graphical interface; optimize your models for accuracy, latency, 
and size; and export them to your application in the cloud, or to an array of devices at the edge.

**Google Cloud’s Vision API**
 offers powerful pre-trained machine learning models
 through REST and RPC APIs. Assign labels to images and quickly classify them 
into millions of predefined categories. 
Detect objects and faces, read printed and handwritten text, 
and build valuable metadata into your image catalog.

**Product is combination of features**

Detect objects automatically
Gain intelligence at the edge
Reduce purchase friction
Understand text and act on it
Detect explicit content
Use our data labeling service

**Product is provided by which company?**
Google Cloud



----------------------------


**2)Product name**
**A.I. Powered Person Detection**

product link :[Link](https://alfredcamera.zendesk.com/hc/en-us/articles/360030851251-A-I-Powered-Person-Detection-Person-Only-)

**Product Short description**

A.I. Powered Person Detection is an Alfred Premium feature that only picks up human action from Motion Detection.
 Using artificial intelligence and machine learning models, Alfred can now differentiate between humans
 from other triggers (e.g. light changes, clouds, and moving trees). What this means for you is that
 Alfred will only send you notifications when it detects human action. 

**Product is combination of features**

Here are some other requirements:

 	Android	iOS	How to
App Version	4.2.0+	2.0.0+	Update Alfred app
OS Version	Android 5.0+	iOS 9+	Update OS version (iOS)
Camera Version	2.0	n/a	Switch to Camera v2.0
CPU	n/a	64 bit	n/a 
OpenGL-ES	3.0+	n/a	Check my device's OpenGL-ES version
Chipset	non-Intel	n/a	Check my device's chipset

**
Motion Detection Schedule is an Alfred Premium feature**
 that allows you to turn on and off Motion Detection at a set time everyday or on weekdays only. Instead of having to remember to turn on Motion Detection manually, you can now more easily monitor your home in the daytime or monitor your store at night to help you feel at ease.
 

The self-learning model is constantly evolving. 
Goal is to provide the most accurate Person Detection model in the home security camera market. 
Help them to make the algorithm smarter by reporting any video that detects anything other than a human.

 